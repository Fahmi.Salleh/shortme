import os
import sqlite3

"""Then, the Database class could be used with with:

with Database() as db:
    db.create_table()

Raises:
    ValueError -- [description]
    IOError -- [description]
    IOError -- [description]
"""


class Database:
    """A Database.
    """

    def __init__(self, db_path=None, **kwargs):
        # If no db_url was provided, fallback to $DATABASE_URL.
        self.__dbpath = db_path or os.environ.get('DATABASE_URL')

        if self.__dbpath:
            # If path doesn't exists
            if not os.path.exists(self.__dbpath):
                raise IOError("Database file '{}'' not found!".format(self.__dbpath))

            # If it's a directory
            if os.path.isdir(self.__dbpath):
                raise IOError("'{}' is a directory!".format(self.__dbpath))

            self.__connect = None
            self.__cursor = None
            self.error = { 'type': None, 'message': None }

            self.db_connect(self.__dbpath)
        else:
            raise ValueError('Database file not define!')



    def __enter__(self):
        return self


    def __exit__(self,exc_type,exc_value,traceback):
        self.db_close()


    def __del__(self):
        self.db_close()


    def db_connect(self, db_path=None):
        try:
            self.__connect = sqlite3.connect(self.__dbpath)
            self.__cursor = self.__connect.cursor()
        except sqlite3.Error as e:
            raise(e)


    def db_close(self):
        if self.__connect:
            #self.__connect.commit()
            self.__cursor.close()
            self.__connect.close()


    def table_list(self):
        sql = "SELECT tbl_name FROM sqlite_master WHERE type = 'table';"
        return self.execute(sql)


    def table_create(self, table, columns):
        sql = "CREATE TABLE IF NOT EXISTS {0} ({1})".format(table, columns)
        return self.execute(sql)


    def table_rename(self, table, newname):
        sql = "ALTER TABLE {0} RENAME TO {1}".format(table, newname)
        return self.execute(sql)


    def table_drop(self, table):
        sql = "DROP TABLE {0}".format(table)
        return self.execute(sql)


    def table_columns(self, table):
        sql = "PRAGMA table_info({0})".format(table)
        recs =  self.execute(sql)
        return [ rec[1] for rec in recs ]


    def sql_select(self, table, columns='*', where=None, group=None, order=None, asc=True):
        sql = "SELECT {0} FROM {1}".format(columns,table)

        if where:
            sql = sql + ' WHERE ' + where

        if group:
            sql = sql + ' GROUP BY ' + group

        if order:
            sql = sql + ' ORDER BY ' + order

        if not asc:
            sql = sql + ' DESC'

        sql = sql + ';'

        return self.execute(sql)


    def sql_insert(self, table, data, datas=[{},{}]):
        columns = ''
        values = ''
        for i in data.strip().split(','):
            info = i.split('=')
            columns += info[0] + ','
            values += info[1] + ','

        sql = "INSERT INTO {0} ({1}) VALUES ({2})".format(table, columns.rstrip(','), values.rstrip(','))
        return self.execute(sql)


    def sql_update(self, table, data, where):
        sql = "UPDATE {0} SET {1} WHERE {2}".format(table, data, where)
        return self.execute(sql)


    def sql_delete(self, table, where):
        sql = "DELETE FROM {0} WHERE {1}".format(table, where)
        return self.execute(sql)


    def sql_query(self, sql):
        return self.execute(sql)


    def execute(self, sql):
        """execute a row of data to current cursor"""
        result = None
        statement = sql.split()[0]
        print("SQL: {}".format(sql))
        #print("TYPE: {}".format(statement))

        try:
            self.__cursor.execute(sql)

            if statement == "SELECT":
                result = self.__cursor.fetchall()
            elif statement == 'INSERT':
                result = self.__cursor.lastrowid
            elif statement == 'UPDATE':
                result = []
            elif statement == 'DELETE':
                result = []

            self.__connect.commit()
        except sqlite3.OperationalError as e:
            self.error['type'] = 'OperationalError'
            self.error['message'] = e
        except sqlite3.IntegrityError as e:
            self.error['type'] = 'IntegrityError'
            self.error['message'] = e
        else:
            return result


    def executemany(self, many_new_data):
        """add many new data to database in one go"""
        self.__cursor.executemany('REPLACE INTO jobs VALUES(?, ?, ?, ?)', many_new_data)


#db = Database('shortme.sqlite3')
#print(db.table_columns('shorten'))
#print(db.sql_select('shorten', order='shortid', asc=False))
#print(db.error)
#print(db.sql_select('shorten', order='shorturl', asc=False))
#print(db.sql_select('shorten', where="shortid=2"))
#id = db.sql_insert("shorten", data="created=datetime('now'), shorturl='NULL', longurl='https://code-maven.com/slides/python-programming/sqlite-update'")
#print(id)

#print(db.sql_update("shorten", data="shorturl='willy'", where="shortid=6"))
#print(db.sql_delete("shorten", where="shortid=11"))
#print(db.sql_query("SELECT * FROM shorten"))
#print("DBERROR: {}".format(db.error))
#print(db.sql_select("shorten", where="shorturl='willy'"))


