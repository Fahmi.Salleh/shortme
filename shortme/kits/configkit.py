import os
import ipaddress
import configparser
import re


class Config:
    """
    Config class implements a basic configuration language which provides
    a structure similar to what’s found in Microsoft Windows INI files.
    You can easily perform:
    1. load default configuration definition.
    2. load configuration from enviroment.
    3. load configuration from file.
    4. Validate all parameters value datatype.
    """

    def __init__(self, configdef, prefix=None):
        """
        Initialize configuration default value (mandatory) and prefix (optional).
        """
        #self.__configfile = configfile or os.environ.get('CONFKIT_CONFIG_FILE')
        self.__configdef = configdef
        self.__prefix = prefix
        self.__configload = None


    def __print_config(self,config):
        """
        Print configuration to console.
        """
        for section in config.sections():
            print()
            print("[{}]".format(section))
            [ print("{} = {}".format(option, value)) for option, value in config.items(section) ]
        print()


    def load_default(self, configdef):
        """
        Load configuration definition default value.
        """
        #print("Loading Default Config")
        try:
            config = configparser.ConfigParser()

            for section in configdef:
                config[section] = {}
                for option in configdef[section]:
                    config[section][option] = configdef[section][option]['value']
        except configparser.ParsingError as err:
            raise IOError('Could not parse: ', err)

        return config


    def load_environment(self, prefix, configdef):
        """
        Load configuration from os shell enviroment.
        """
        #print("Loading Evironment Config")
        if prefix:
            try:
                config = configparser.ConfigParser()

                for section in configdef:
                    config[section] = {}
                    for option in configdef[section]:
                        osvar = "{}_{}_{}".format(prefix.upper(), section.upper(), option.upper())
                        osval = os.environ.get(osvar)
                        if osval:
                            config[section][option] = osval
            except configparser.ParsingError as err:
                raise IOError('Could not parse: ', err)
        else:
            raise IOError("Config environment marker not set!")

        return config


    def load_file(self, configfile):
        """
        Load configurations from file.
        """
        #print("Loading File Config")
        config = None

        if os.path.isfile(configfile):
            try:
                config = configparser.ConfigParser()
                config.read(configfile)

                #self.__print_config(config)

            except configparser.ParsingError as err:
                raise IOError('Could not parse: ', err)
        else:
            raise IOError("Config file '{0}'' not found!".format(configfile))

        return config


    def load(self, configfile=None):
        """
        Load configuration priority by sequence:
        1. Default configuration from definition will be load.
        2. Environment configuration will be load and overiding default configuration.
        3. File configuration will be load and overiding environment configuration.
        """
        # Load default config value
        config = self.load_default(self.__configdef)

        # Overide default config value with os config value
        if self.__prefix:
            loadenv = self.load_environment(self.__prefix, self.__configdef)

            for section in loadenv.sections():
                for option in loadenv.options(section):
                    value = loadenv.get(section, option)
                    config[section][option] = value

        # Overide os config value with file config value
        if configfile:
            loadfile = self.load_file(configfile)

            for section in loadfile.sections():
                for option in loadfile.options(section):
                    value = loadfile.get(section, option)
                    config[section][option] = value

        #self.__print_config(config)

        # Validate all parameters datatype.
        self.validate(configload=config, configdef=self.__configdef)

        self.__configload = config

        return self.__configload

    def save(self, configfile, configload):
        """
        Save a configuration to .ini file.
        """
        with open(configfile, 'w') as file:
            configload.write(file)

    def validate(self, configload, configdef):
        """
        Validate parameters value data type with default config
        """
        for section in configload.sections():
            for parameter in configload.options(section):
                value = configload.get(section, parameter)

                if not value:
                    raise ValueError("CONFIG: Parameter {0}->{1} can not be empty!".format(section, parameter))

                datatype = configdef[section][parameter]['datatype']

                if datatype == 'alphanum':
                    if not value.isalnum():
                        raise ValueError("CONFIG: Parameter {0}->{1} value '{2}' is not alphanumeric!".format(section, parameter, value))
                elif datatype == 'alpha':
                    if not value.isalpha():
                        raise ValueError("CONFIG: Parameter {0}->{1} value '{2}' is not alphabet!".format(section, parameter, value))
                elif datatype == 'integer':
                    if not value.isdigit():
                        raise ValueError("CONFIG: Parameter {0}->{1} value '{2}' is not numeric!".format(section, parameter, value))
                elif datatype == 'bool':
                    if not value == "True" and not value == "False":
                        raise ValueError("CONFIG: Parameter {0}->{1} value '{2}' is not boolean!".format(section, parameter, value))
                elif datatype == 'hostip':
                    try:
                        ipaddress.ip_address(value)
                    except ValueError:
                        if not self.is_valid_hostname(value):
                            raise ValueError("CONFIG: Parameter {0}->{1} value '{2}' is not valid ip address or hostname!".format(section, parameter, value))
                elif datatype == 'file':
                    try:
                        open(value, 'r')
                    except OSError:
                        raise ValueError("CONFIG: Parameter {0}->{1} value '{2}', file is not found!".format(section, parameter, value))
                elif datatype == 'open':
                    pass
                else:
                    raise ValueError("CONFIG: Parameter {0}->{1} datatype '{2}' is not recognized!".format(section, parameter, value))


    def is_valid_hostname(self, hostname):
        """
        Validate hostname and ipv4 & ipv6 address format.
        """
        if len(hostname) > 255:
            return False
        if hostname[-1] == ".":
            hostname = hostname[:-1] # strip exactly one dot from the right, if present
        allowed = re.compile("(?!-)[A-Z\d-]{1,63}(?<!-)$", re.IGNORECASE)
        return all(allowed.match(x) for x in hostname.split("."))


    def is_valid_email(self, email):
        """
        Validate email format.
        """
        match = re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', email)

        if match == None:
            return False
        else:
            return True


    def is_valid_password(self, password):
        """
        Validate password with minimum rules below:
        1. Should have at least one number.
        2. Should have at least one uppercase and one lowercase character.
        3. Should have at least one special symbol =['$', '@', '#', '%'].
        4. Should be between 8 to 20 characters long.
        """
        reg = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!#%*?&]{8,20}$"

        # compiling regex
        pattern = re.compile(reg)

        # searching regex
        match = re.search(pattern, password)

        # validating conditions
        if match:
            return True
        else:
            return False


confdef = {
    'SERVER': {
        'host': { 'datatype': 'hostip', 'value': '127.0.0.1' },
        'port': { 'datatype': 'integer', 'value': '8080' }
    },
    'SECURITY': {
        'hashsalt': { 'datatype': 'open', 'value': 'salt can be your login id or login name' }
     },
    'DATABASE': {
        'type': { 'datatype': 'alphanum', 'value': 'sqlite3' },
        'name': { 'datatype': 'file', 'value': 'shortme.sqlite3' },
        'commit': { 'datatype': 'alpha', 'value': 'auto' }
    },
    'DEVELOPMENT': {
        'debug': { 'datatype': 'bool', 'value': 'True' },
        'autoreload': { 'datatype': 'bool', 'value': 'True' }
    }

}


"""
# LOAD DEFAULT CONFIG ONLY
myconf1 = Config(confdef)
print(f"SERVER PORT DEFAULT     : {myconf1.load()['SERVER']['port']}")
# SERVER PORT : 8080

# LOAD ENVIRONMENT CONFIG ONLY
# on os shell: export SHORTME_SERVER_PORT=8000
myconf2 = Config(confdef, prefix='SHORTME')
print(f"SERVER PORT ENV         : {myconf2.load().get('SERVER', 'port')}")
# SERVER PORT : 8000

# LOAD FILE CONFIG ONLY
myconf3 = Config(confdef)
print(f"SERVER PORT FILE        : {myconf3.load(configfile='config.ini')['SERVER']['port']}")
# SERVER PORT : 80

# LOAD ENVIRONMENT & FILE CONFIG
myconf4 = Config(confdef, prefix='SHORTME')
print(f"SERVER PORT ENV & FILE  : {myconf4.load(configfile='config.ini')['SERVER']['port']}")

#Save default config definition to file
myconf5 = Config(confdef, prefix='SHORTME')
myconf5.save(configfile='confignew.ini', configload=myconf5.load())

#print(myconf.get('SERVER', 'port'))
#confdef = configparser.ConfigParser()
#confdef.read_dict(struct)
#print(confdef.sections())

#onffile = loadconfig('config.ini')

#validate(conffile, confdef)
#checkempty(conf)
"""

#print(conf)

#print(conf['SERVER']['HOST'])

#config = configparser.ConfigParser()
#print(struct)
#if not conf['SERVER']['HOST']:
#    raise ValueError('ERROR: Port was not configured.')


