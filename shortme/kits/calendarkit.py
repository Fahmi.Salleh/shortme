import datetime

class Week:
    "Calendar made easy"
    def __init__(self, weekdate=None):
        "Initialize"
        if weekdate:
            try :
                self.dateinput = datetime.datetime.strptime(weekdate, '%Y-%m-%d')
            except (ValueError, TypeError):
                raise ValueError("Incorrect date format, should be 'YYYY-MM-DD'.")
        else:
            self.dateinput = weekdate

    def number(self, weekdate=None):
        """Return number of week that date sitting on"""

        if weekdate is None and self.dateinput is None:
            raise TypeError("Argument local or global 'weekdate' required!")

        if weekdate:
            weeknum = datetime.datetime.strptime(weekdate, '%Y-%m-%d').isocalendar()[1]
        else :
            weeknum = self.dateinput.isocalendar()[1]

        return weeknum

    def start_date(self, weekdate=None, weeknum=None, strformat=True):
        """Return date of start week. Week start on Monday.
        inputweek format %Y-W%W. Example: Year 2019 week 18 = 2019-W18"""

        if weekdate is None and weeknum is None and self.dateinput is None:
            raise TypeError("Argument local/global 'weekdate' or local 'weeknum' required!")
        if weekdate is not None and weeknum is not None:
            raise TypeError("Can't have both weekdate or weeknum argument!")

        if weekdate:
            dateinput = datetime.datetime.strptime(weekdate, '%Y-%m-%d')
            weekstart = dateinput - datetime.timedelta(days=dateinput.weekday())
        elif weeknum:
            # Get Week start date by week number
            # -1 and -%w pattern tells the parser to pick the Monday in that week.
            # %W uses Monday as the first day of the week.
            # %G-W%V-%u week number is a ISO week date, require Python 3.6 or newer.
            # %G-W%V-%u are ISO equivalents of "%Y-W%W-%w"
            weekstart = datetime.datetime.strptime(weeknum + '-1', "%G-W%V-%u")
        else:
            # Get Week start date by week date
            weekstart = self.dateinput - datetime.timedelta(days=self.dateinput.weekday())

        if strformat:
            weekstart = weekstart.strftime('%Y-%m-%d')

        return weekstart

    def last_date(self, weekdate=None, weeknum=None, strformat=True):
        """Return date of end week. Week end on Sunday."""

        if weekdate is None and weeknum is None and self.dateinput is None:
            raise TypeError("Argument local/global 'weekdate' or local 'weeknum' required!")
        if weekdate is not None and weeknum is not None:
            raise TypeError("Can't have both weekdate or weeknum argument!")

        if weekdate:
            weekstart = self.start_date(weekdate=weekdate, strformat=False)
        elif weeknum:
            weekstart = self.start_date(weeknum=weeknum, strformat=False)
        else:
            weekstart = self.start_date(weekdate=datetime.datetime.strftime(self.dateinput, '%Y-%m-%d'), strformat=False)

        weeklast = weekstart + datetime.timedelta(days=6)

        if strformat:
            weeklast = weeklast.strftime('%Y-%m-%d')
        return weeklast

    def list_date(self, weekdate=None, weeknum=None):
        """Return list of dates in a week"""
        if weekdate is None and weeknum is None and self.dateinput is None:
            raise TypeError("Argument local/global 'weekdate' or local 'weeknum' required!")
        if weekdate is not None and weeknum is not None:
            raise TypeError("Can't have both weekdate or weeknum argument!")

        if weekdate:
            startdate = self.start_date(weekdate=weekdate, strformat=False)
        elif weeknum:
            startdate = self.start_date(weeknum=weeknum, strformat=False)
        else:
            startdate = self.start_date(strformat=False)

        dateslist = []

        for i in range(0, 7):
            datecalc = startdate + datetime.timedelta(days=i)
            dateslist.append(datecalc.strftime('%Y-%m-%d'))

        return dateslist


class Month:
    "Calendar made easy"
    def __init__(self, month=None):
        """Initialize"""
        if month:
            self.__month_check_format(month)
            self.monthinput = datetime.datetime.strptime(month, '%Y-%m')
        else:
            self.monthinput = None


    def last_date(self, month=None, strformat=True):
        """Takes a datetime.date and returns the date for the last day in the same month."""
        if month is None and self.monthinput is None:
            raise TypeError("Argument local or global 'monthdate' required!")

        if month:
            self.__month_check_format(month)
            monthfirstdate = self.___month_first_date(month)
            monthnext = datetime.datetime.strptime(monthfirstdate, '%Y-%m-%d').replace(day=28) + datetime.timedelta(days=4)
        else:
            monthnext = self.monthinput.replace(day=28) + datetime.timedelta(days=4)

        monthlast = monthnext - datetime.timedelta(days=monthnext.day)
        if strformat:
            monthlast = monthlast.strftime('%Y-%m-%d')

        return monthlast


    def __month_check_format(self, monthdate):
            try :
                # Check month date input if correct format
                mdate = datetime.datetime.strptime(monthdate, '%Y-%m')
            except (ValueError, TypeError):
                raise ValueError("Incorrect date format, should be 'YYYY-MM'.")


    def ___month_first_date(self, monthdate):
            # Convert back to string with date 01 (month always begin with 01)
            #return datetime.datetime.strftime(monthdate, '%Y-%m-01')
            return "{}-01".format(monthdate)


    def list_week(self, month=None):
        """List weeks number in targeted month."""
        if month is None and self.monthinput is None:
            raise TypeError("Argument local or global 'monthdate' required!")

        if month:
            self.__month_check_format(month)
            monthfirstdate = self.___month_first_date(month)
            start_weeknum = Week(weekdate=monthfirstdate).number()
            last_weeknum = Week(weekdate=self.last_date(month=month)).number()
        else:
            start_weeknum = Week(weekdate=datetime.datetime.strftime(self.monthinput, '%Y-%m-01')).number()
            last_weeknum = Week(weekdate=self.last_date()).number()

        weeklist = []

        for w in range(start_weeknum, last_weeknum + 1):
            weeklist.append(w)

        return weeklist


    def list_date(self, month=None):
        """List weeks number and dates in targeted month."""
        if month is None and self.monthinput is None:
            raise TypeError("Argument local or global 'monthdate' required!")

        if month:
            self.__month_check_format(month)
            # Targeted month to retrive date
            monthtarget = month
            # Get weeks belong to targeted month
            weeklist = self.list_week(month=month)
            # Set formating for weeknum
            weekformat = month.split('-')[0] + '-W'
        else:
            monthtarget = datetime.datetime.strftime(self.monthinput, '%Y-%m')
            # Get weeks belong to targeted month
            weeklist = self.list_week()
            # Set formating for weeknum
            weekformat = self.monthinput.strftime('%Y-W')

        wdlist = []

        for week in weeklist:
            # Get list of dates from week number
            tdatelist  = Week().list_date(weeknum=weekformat + str(week))
            # Remove dates which not belong to targeted month
            datelist = [ date if date.rsplit('-', 1)[0] == monthtarget else '' for date in tdatelist ]
            # Stich together week number and dates for targeted month
            wdlist.append(tuple((week, datelist)))

        return wdlist


"""
now = datetime.datetime.today().strftime('%Y-%m-%d')
print("Today date: {}".format(now))
print()
print("GLOBAL DATE SET")
w = Week(now)
print("Week Number: " + str(w.number()))
print("Week Start Date (global date): " + w.start_date())
print("Week Start Date (local date): " + w.start_date(weekdate=now))
print("Week Start Date (local numweek): " + w.start_date(weeknum='2019-W19'))
print("Week Last Date (global date): " + w.last_date())
print("Week Last Date (local date): " + w.last_date(weekdate=now))
print("Week Last Date (local numweek): " + w.last_date(weeknum='2019-W19'))
print("Week Date List (global date): " + str(w.list_date()))
print("Week Date List (local date): " + str(w.list_date(weekdate=now)))
print("Week Date List (local numweek): " + str(w.list_date(weeknum='2019-W19')))

print()
print("GLOBAL DATE NOT SET")
w2 = Week()
print("Week Number: " + str(w2.number('2019-05-01')))
#print("Week Start Date (global date): " + w2.start_date())
print("Week Start Date (local date): " + w2.start_date('2019-05-01'))
print("Week Start Date (local numweek): " + w2.start_date(weeknum='2019-W18'))
#print("Week Last Date (global date): " + w2.last_date())
print("Week Last Date (local date): " + w2.last_date('2019-05-01'))
print("Week Last Date (local numweek): " + w2.last_date(weeknum='2019-W18'))
#print("Week Date List (global date): " + str(w2.list_date()))
print("Week Date List (local date): " + str(w2.list_date('2019-05-01')))
print("Week Date List (local numweek): " + str(w2.list_date(weeknum='2019-W18')))


now = datetime.datetime.today().strftime('%Y-%m')
print("Today date: {}".format(now))

print()
print("GLOBAL DATE SET")
m = Month(now)
print("Month Last Date: " + m.last_date())
print("Month Week List: " + str(m.list_week()))
print("Month Date List: " + str(m.list_date()))

print()
print("GLOBAL DATE NOT SET")
m2 = Month()
print("Month Last Date: " + m2.last_date('2019-05'))
print("Month Week List: " + str(m2.list_week('2019-05')))
print("Month Date List: " + str(m2.list_date('2019-05')))
"""