#!/usr/bin/env python
#
# -*- mode:python; sh-basic-offset:4; indent-tabs-mode:nil; coding:utf-8 -*-
# vim:set tabstop=4 softtabstop=4 expandtab shiftwidth=4 fileencoding=utf-8:
#

import sys
import os
import datetime
import re
import bottle
import configdef
from hashids import Hashids
from ua_parser import user_agent_parser
from kits import configkit
from kits import calendarkit
from kits import sqlite3kit

# Change working directory so relative paths (and template lookup) work again
if os.path.dirname(__file__):
    os.chdir(os.path.dirname(__file__))

config = configkit.Config(configdef.definition, 'SHORTME').load('shortme.conf')

sitedomain = config['SERVER']['host'] + ':' + str(config['SERVER']['port'])

dbobj = sqlite3kit.Database(config['DATABASE']['name'])


@bottle.get('/')
@bottle.get('/index.html')
def home():
    return bottle.template('index.html')


@bottle.post('/shorten')
def shorten():
    urllong = str(bottle.request.forms.get('url'))
    print("longurl: " + urllong)

    if url_is_valid(urllong):
        hashids = Hashids(salt=config['SECURITY']['hashsalt'])

        #Insert new url to database
        urlid = dbobj.sql_insert(table="shorten", data="created=datetime('now'), shorturl='NULL', longurl='{0}'".format(urllong))

        if urlid:
            print("urlid: " + str(urlid))
            hashurlid = hashids.encode(urlid)
            dbobj.sql_update(table="shorten", data="shorturl='{0}'".format(hashurlid), where="shortid={0}".format(str(urlid)))
        else:
            hashurlid = dbobj.sql_select(table="shorten", columns="shorturl", where="longurl='{0}'".format(str(urllong)))[0][0]

            print("URL exist: " + urllong)
            print("hashurlid: " + str(hashurlid))

        urlshort = "http://{}/{}".format(sitedomain, hashurlid)
        # retrieve long url from db
        print("shorturl: " + urlshort)
        shortstats = "http://{0}/stats/{1}/today".format(sitedomain, hashurlid)

        return bottle.template('index.html', longurl=urllong, shorturl=urlshort, shortstats=shortstats)
    else:
        return bottle.template('index.html')


@bottle.get('/<shorturl>')
def redirect_url(shorturl):
    print("redirect short url:" + str(shorturl))

    rec = dbobj.sql_select(table="shorten", columns="shortid, longurl", where="shorturl='{0}'".format(str(shorturl)))

    if rec:
        shortid = rec[0][0]
        longurl = str(rec[0][1])
        print("shortid: " + str(shortid))
        print("longurl: " + str(longurl))

        useragent = bottle.request.headers.get('User-agent')
        br = user_agent_parser.ParseUserAgent(useragent)
        os = user_agent_parser.ParseOS(useragent)
        #print("Device: " + str(user_agent_parser.ParseDevice(useragent)))

        #referer = request.headers.get('Referer')
        refproxy = bottle.request.environ.get('HTTP_X_FORWARDED_FOR')
        rerdirect = bottle.request.environ.get('REMOTE_ADDR')

        if refproxy:
            referer = refproxy
        elif rerdirect:
            referer = rerdirect
        else:
            referer = 'None'

        dbobj.sql_insert(table="accessinfo", data="shortid={0}, recorded=datetime('now'), brfamily='{1}', brmajor='{2}', brminor='{3}', osfamily='{4}', osmajor='{5}', osminor='{6}', referer='{7}'".format(shortid, br['family'], br['major'], br['minor'], os['family'], os['major'], os['minor'], referer))

        bottle.redirect(longurl)
    else:
        print("Shorten URL not found! Redirect to 404 page")
        bottle.abort(404, "Shorten URL not found!")


@bottle.get('/stats/<shorthash>/<statview>')
def pagestats(shorthash, statview, method='GET'):

    rec = dbobj.sql_select(table="shorten", columns="shortid, longurl", where="shorturl='{0}'".format(shorthash))

    if rec:
        shortid = rec[0][0]
        longurl = rec[0][1]
        shorturl = "http://{0}/{1}".format(sitedomain, shorthash)

        today = datetime.datetime.today()
        # Yearly
        # SELECT * FROM accessinfo WHERE strftime('%Y', recorded) = '2019'";
        # Previous month
        # SELECT * FROM accessinfo WHERE recorded >= date('now','start of month','-1 month') AND recorded < date('now','start of month')

        #rec = dbobj.sql_select(table="accessinfo", columns="strftime('%Y', recorded) AS recyear, COUNT(*) AS clicks", where="shortid={0}".format(shortid))
        #dbcur.execute("SELECT strftime('%Y', recorded) AS recyear, COUNT(*) AS clicks FROM accessinfo WHERE shortid = " + str(shortid) + " AND recyear = '2019'")
        #recweek = dbobj.sql_select(table="accessinfo_this_week", where="shortid={0}".format(shortid))
        if statview == "today":
            clickstitle = "Clicks by hours for today {}".format(today.strftime('%d, %b %Y'))
            clicksxaxes = "HOUR (UTC)"
            clickdate = [ "{0:0>2}".format(h) for h in range(0, 24) ]
            # Select records by today
            records = dbobj.sql_query("SELECT * FROM accessinfo WHERE shortid={0} AND STRFTIME('%Y-%m-%d', recorded) = STRFTIME('%Y-%m-%d', '{1}')".format(shortid, today.strftime('%Y-%m-%d')))
        elif statview == "week":
            ck = calendarkit.Week(today.strftime('%Y-%m-%d'))
            clickdate = ck.list_date()
            clickstitle = "Clicks by dates for week number {}".format(ck.number())
            clicksxaxes = "DATE"
            # Select records by target week
            records = dbobj.sql_query("SELECT * FROM accessinfo WHERE shortid={0} AND DATE(recorded) > DATE('now', 'weekday 0', '-7 days')".format(shortid))
        elif statview == "month":
            clickdate = calendarkit.Month().list_date(today.strftime('%Y-%m'))
            targetmonth = today.strftime('%Y-%m')
            clickstitle = "Clicks by weeks number for {}".format(today.strftime('%b %Y'))
            clicksxaxes = "WEEK"
            # Select records by target month
            records = dbobj.sql_query("SELECT * FROM accessinfo WHERE shortid={0} AND STRFTIME('%Y-%m', recorded) = '{1}'".format(shortid, targetmonth))
        elif statview == "year":
            clickstitle = "Clicks by months for year {}".format(today.strftime('%Y'))
            targetyear = today.strftime('%Y')
            clickdate = [ "{0:0>2}".format(m) for m in range(1, 13) ]
            clicksxaxes = "MONTH"
            # Select records by target year
            records = dbobj.sql_query("SELECT * FROM accessinfo WHERE shortid={0} AND STRFTIME('%Y', recorded) = '{1}'".format(shortid, targetyear))

        clickstotal = len(records)
        clickslabel, clicksdata = clicks_stat(records, clickdate, statview)
        reflabel, refdata = refferers_stat(records)
        browserlabel, browserdata = browser_stat(records)
        oslabel, osdata = os_stat(records)
    else:
        bottle.abort(404, "Shorten URL not found!")

    return bottle.template('stats.html',
                    shorthash=shorthash,
                    longurl=longurl,
                    shorturl=shorturl,
                    viewactive=statview,
                    clickstotal=clickstotal,
                    clickslabel=clickslabel,
                    clicksdata=clicksdata,
                    clicksxaxes=clicksxaxes,
                    clickstitle=clickstitle,
                    reflabel=reflabel,
                    refdata=refdata,
                    browserlabel=browserlabel,
                    browserdata=browserdata,
                    oslabel=oslabel,
                    osdata=osdata
                )


def clicks_stat(records, datelist, statview):
    clickslabel = []
    clicksdata = []

    if statview == "today":
        clickslabel = [ "{}:00".format(x) for x in datelist ]
        for h in datelist:
            clickcount = 0
            for r in records:
                hour = r[2].split()[1].split(':')[0]
                if h == hour:
                    clickcount += 1
            clicksdata.append(clickcount)
    elif statview == "week":
        clickslabel = [ wd.split('-')[2] for wd in datelist ]
        for d in datelist:
            clickcount = 0
            for r in records:
                if datetime.datetime.strptime(d, '%Y-%m-%d') == datetime.datetime.strptime(r[2].split()[0], '%Y-%m-%d'):
                    clickcount += 1
            clicksdata.append(clickcount)
    elif statview == "month":
        for wdata in datelist:
            clickslabel.append(wdata[0])
            clickcount = 0
            # Filter to remove empty string
            for date in filter(None, wdata[1]):
                for r in records:
                    if datetime.datetime.strptime(date, '%Y-%m-%d') == datetime.datetime.strptime(r[2].split()[0], '%Y-%m-%d'):
                        clickcount += 1
            clicksdata.append(clickcount)
    elif statview == "year":
        clickslabel = datelist
        # Iterate each month
        for month in clickslabel:
            clickcount = 0
            # Iterate each record.
            for r in records:
                # Check if month is the same and count
                if month == r[2].split('-')[1]:
                    clickcount += 1
            clicksdata.append(clickcount)

    return clickslabel, clicksdata


def refferers_stat(records):
    refdict = {}
    totalclick = len(records)

    for r in records:
        if r[9] not in refdict:
            refdict[r[9]] = 0
        refdict[r[9]] += 1

    refsorted = sorted([(value, key) for (key, value) in refdict.items()], reverse=True)

    reflabel, refdata = [], []
    totaltop5click = 0

    # just take top 5
    for r in refsorted[0:5]:
        refdata.append(r[0])
        reflabel.append(r[1])
        totaltop5click += r[0]

    if totalclick != totaltop5click:
        refdata.append(totalclick - totaltop5click)
        reflabel.append("Others")

    return reflabel, refdata


def browser_stat(records):
    safari, chrome, firefox, edge, others = 0, 0, 0, 0, 0

    for r in records:
        if r[3] == "Safari":
            safari += 1
        elif r[3] == "Chrome":
            chrome += 1
        elif r[3] == "Firefox":
            firefox += 1
        elif r[3] == "Edge":
            edge += 1
        else:
            others += 1

    browserlabel = ['Safari', 'Chrome', 'Firefox', 'Edge', 'Others']
    browserdata =  [safari, chrome, firefox, edge, others]
    return browserlabel, browserdata


def os_stat(records):
    macosx, windows, ios, android, others = 0, 0, 0, 0, 0

    for r in records:
        if r[6] == "Mac OS X":
            macosx += 1
        elif r[6] == "Windows":
            windows += 1
        elif r[6] == "iOS":
            ios += 1
        elif r[6] == "Android":
            android += 1
        else:
            others += 1

    oslabel = ['MacOSX', 'Windows', 'iOS', 'Android', 'Others']
    osdata = [macosx, windows, ios, android, others]
    return oslabel, osdata


@bottle.get('/<filename:path>')
def send_static(filename):
    return bottle.static_file(filename, root="static/")


@bottle.get('/restricted')
def restricted():
    bottle.error(401, "Sorry, access denied.")


@bottle.error(404)
def error404(msg):
    return bottle.template('error404.html', errmsg=msg)

def url_is_valid(urlstring):
    urlisvalid = False

    REGEXURL = re.compile(
        u"^"
        # protocol identifier
        u"(?:(?:https?|ftp)://)"
        # user:pass authentication
        u"(?:\S+(?::\S*)?@)?"
        u"(?:"
        # IP address exclusion
        # private & local networks
        u"(?!(?:10|127)(?:\.\d{1,3}){3})"
        u"(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})"
        u"(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})"
        # IP address dotted notation octets
        # excludes loopback network 0.0.0.0
        # excludes reserved space >= 224.0.0.0
        # excludes network & broadcast addresses
        # (first & last IP address of each class)
        u"(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])"
        u"(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}"
        u"(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))"
        u"|"
        # host name
        u"(?:(?:[a-z\u00a1-\uffff0-9]-?)*[a-z\u00a1-\uffff0-9]+)"
        # domain name
        u"(?:\.(?:[a-z\u00a1-\uffff0-9]-?)*[a-z\u00a1-\uffff0-9]+)*"
        # TLD identifier
        u"(?:\.(?:[a-z\u00a1-\uffff]{2,}))"
        u")"
        # port number
        u"(?::\d{2,5})?"
        # resource path
        u"(?:/\S*)?"
        u"$"
        , re.UNICODE)

    if re.match(REGEXURL, urlstring) is not None:
        urlisvalid = True
    return urlisvalid


# used for python shortme.py
if __name__ == '__main__':
    # Starts a local test server.
    bottle.run(
                host=config['SERVER']['host'],
                port=config['SERVER']['port'],
                server=config['SERVER']['adapter'],
                reloader=config['DEVELOPMENT']['autoreload'],
                debug=config['DEVELOPMENT']['debug']
            )


# this is the hook for Gunicorn or Apache mod_wsgi to run Bottle
# gunicorn -w 2 shortme:app
app = bottle.default_app()