definition = {
    'SERVER': {
        'host': { 'datatype': 'hostip', 'value': '127.0.0.1' },
        'port': { 'datatype': 'integer', 'value': '8000' },
        'adapter': { 'datatype': 'alpha', 'value': 'auto' }
    },
    'SECURITY': {
        'hashsalt': { 'datatype': 'open', 'value': 'salt can be your login id or login name' }
     },
    'DATABASE': {
        'type': { 'datatype': 'alphanum', 'value': 'sqlite3' },
        'name': { 'datatype': 'file', 'value': 'shortme.sqlite3' },
        'commit': { 'datatype': 'alpha', 'value': 'auto' }
    },
    'DEVELOPMENT': {
        'debug': { 'datatype': 'bool', 'value': 'False' },
        'autoreload': { 'datatype': 'bool', 'value': 'False' }
    }
}