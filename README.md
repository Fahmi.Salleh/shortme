# ShortMe

Convert long and ugly URLS into short and attractive urls. Make them easier to share.


## Screenshot

![ShortMe in action...](docs/screenshot/shortme-webshot2019-04-04.png)*ShortMe screenshot*


## Planned Features

* [x]  Generate Shorten URL from Original URL
* [x]  Redirect Shorten URL to Original URL
* [ ]  URL Statistic (clicks per hours, days, months, unique click) 
* [ ]  URL Analytics (refferers, platform, browser, countries)
* [ ]  User Login & Private URL Statistic and Analytics


## Technology Used

For those interested, technology used in this project includes:

*  [Python 3.7](https://www.python.org) - Python is a programming language that lets you work quickly
and integrate systems more effectively.
*  [Bottle](https://bottlepy.org/docs/0.12/#) - Fast, simple and lightweight WSGI micro web-framework for Python.
*  [Bulma](https://bulma.io) - Free, open source CSS framework based on Flexbox
*  [ClipboardJS](https://clipboardjs.com ) - A modern approach to copy text to clipboard
*  [SQLite](https://www.sqlite.org) - SQLite is a small, fast, self-contained, high-reliability, full-featured, SQL database engine. 


## License

This project is licensed under the MIT License - see the LICENSE file for details.
